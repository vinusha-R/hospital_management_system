package com.patient;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

import com.admin.AdminOptions;
import com.database.Database;
import com.hms.Account;
import com.hms.HospitalSvims;
import com.hms.IdGenerator;

public class PatientOptions extends IdGenerator {
	String id = null;
	Scanner scanner = new Scanner(System.in);
	Connection con = Database.connectDb();

	public void display() {
		System.out.println("choose your option");
		System.out.println("1:Login");
		System.out.println("2:Register");
		int option = scanner.nextInt();
		switch (option) {
		case 1:
			login();
			break;
		case 2:
			register();
			break;
		}
	}

	public void login() {
		String pwd = null;
		String password = null;
		System.out.println("enter your id");
		String myId = scanner.next();
		ArrayList<String> arrayList = new ArrayList();
		Connection con = Database.connectDb();
		try {
			Statement statement = con.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from patient");
			while (resultSet.next()) {
				arrayList.add(resultSet.getString(1));
			}
			if (arrayList.contains(myId)) {
				ResultSet patientResult = statement.executeQuery("select * from patient where pid='" + myId + "'");
				patientResult.next();
				pwd = patientResult.getString(5);
				System.out.println("enter your password");
				password = scanner.next();
			} else {
				System.out.println("invalid id");
				System.out.println("enter valid id or if new user register");
				display();
			}
			if (pwd.equals(password)) {
				id = myId;
				System.out.println("welcome");
				while (true) {
					options();
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void register() {
		String docId = null;
		String dateNew = null;
//		String status = "No";
//		System.out.println("enter your id");
//		int id = sc.nextInt();
		IdGenerator adminId = new AdminOptions();
		int id = adminId.generateId("patient");
//		System.out.println(id);
		String newId = "p" + id;
		System.out.println("enter your name");
		String name = scanner.next();
		System.out.println("enter your password");
		String pwd = scanner.next();
		System.out.println("Available departments are");
		try {
			Statement statement = con.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from doctor");
			HashSet hashSet = new HashSet();
			while (resultSet.next()) {
				// System.out.println(rs.getString(3));
				hashSet.add(resultSet.getString(3));
			}
			System.out.println(hashSet);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String dept = scanner.next();
		try {
			Statement statement = con.createStatement();
			ResultSet doctorResult = statement.executeQuery("select * from doctor where specialization='" + dept + "'");
			if (doctorResult.next()) {
				System.out.println("choose the date and doctor id");
				System.out.println("doctors and doctor available dates are");
				// System.out.println("docId" + " || " + "availability");
				System.out.format("%15s%15s\n", "docId", "availability");
				System.out.println("------------------------------------");
				doctorResult.beforeFirst();
				while (doctorResult.next()) {
					// System.out.println(rs1.getInt(1) + " || " + rs1.getString(4));
					System.out.format("%15s%15s\n", doctorResult.getString(1), doctorResult.getString(4));
				}
				docId = scanner.next();
				dateNew = scanner.next();
			} else {
				System.out.println("doctor is not available");
				System.out.println("you will be updated once doctor is available");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
//			Statement st = con.createStatement();
//			int a = st.executeUpdate("insert into patient(pid,pname,department,status,docid,ppwd,pdate)values(" + id
//					+ ",'" + name + "','" + dept + "','" + status + "'," + docid + ",'" + pwd + "','" + date1+"'");
			String sql = "insert into patient(pid,pname,department,status,docid,ppwd,pdate)values(?,?,?,?,?,?,?)";
			PreparedStatement preparedStatement = con.prepareStatement(sql);
			preparedStatement.setString(1, newId);
			preparedStatement.setString(2, name);
			preparedStatement.setString(3, dept);
			preparedStatement.setBoolean(4, false);
			preparedStatement.setString(5, docId);
			preparedStatement.setString(6, pwd);
			preparedStatement.setString(7, dateNew);
			preparedStatement.executeUpdate();
			System.out.println("your registration has been successful");
			System.out.println("your id is :" + newId);
			System.out.println("login here");
			login();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void options() {
		System.out.println("choose your option");
		System.out.println("1:Check doctor availabilty");
		System.out.println("2:Doctor consultation date");
		System.out.println("3:View the test report");
		System.out.println("4:Exit");
		int option = scanner.nextInt();
		switch (option) {
		case 1:
			checkDocAvailability();
			break;
		case 2:
			myConsultationDate(id);
			break;
		case 3:
			viewReport(id);
			break;
		case 4:
			HospitalSvims.svimsDisplay();
		}
	}

	public void checkDocAvailability() {
		System.out.println("enter the department");
		String dept = scanner.next();
		Statement statement;
		try {
			statement = con.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from doctor where specialization='" + dept + "'");
//			System.out.println("docId" + " || " + "docName" + " || " + "Available date");
			System.out.format("%10s%15s%20s\n", "docId", "docdName", "Available-date");
			System.out.println("------------------------------------------------");
			while (resultSet.next()) {
//				System.out.println(rs.getInt(1) + "   || " + rs.getString(2) + "  || " + rs.getString(4));
				System.out.format("%10s%15s%20s\n", resultSet.getString(1), resultSet.getString(2), resultSet.getString(4));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void myConsultationDate(String id) {
		try {
			Statement statement = con.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from patient where pid='" + id + "'");
			resultSet.next();
			if ((resultSet.getBoolean(8)) == false) {
				if ((resultSet.getString(6)).equals(null)) {
					System.out.println("you have not been assigned consultation date yet");
				} else if ((resultSet.getBoolean(7)) == false) {
					System.out.println("you have been assigned with consultation date but not confirmed yet");
					System.out.println("Consultation date is: " + resultSet.getString(6));
				} else {
					System.out.println("Your consultation date is:" + resultSet.getString(6));
				}
			} else {
				System.out.println("your consultation has been done on :" + resultSet.getString(6));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void viewReport(String patientId) {
		try {
			Statement statement = con.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from patientreport where pid='" + patientId + "'");
			Statement statementNew = con.createStatement();
			ResultSet patientResult = statementNew.executeQuery("select * from patient");
			ArrayList<String> patientIdList = new ArrayList();
			while (patientResult.next()) {
				patientIdList.add(patientResult.getString(1));
			}
			if (patientIdList.contains(patientId)) {
				if (resultSet.next()) {
//					System.out.println("reportId" + " || " + "diagnosed" + " || " + "pbs" + " || " + "cbcs" + " || "
//							+ "antigen" + " || " + "pid");
					System.out.format("%10s%13s%8s%8s%10s%10s\n", "reportId", "diagnosed", "pbs", "cbcs", "antigen",
							"pid");
					System.out.println("------------------------------------------------------------------");
					resultSet.beforeFirst();
					while (resultSet.next()) {
//						System.out.println(rs.getInt(1) + " || " + rs.getString(2) + " || " + rs.getString(3) + " || "
//								+ rs.getString(4) + " || " + rs.getString(5) + " || " + rs.getString(6));
						System.out.format("%10s%10s%10s%10s%10s%10s\n", resultSet.getString(1), resultSet.getString(2),
								resultSet.getString(3), resultSet.getString(4), resultSet.getString(5), resultSet.getString(6));
					}
				} else {
					System.out.println("Report is not generated yet");
					options();
				}
			} else {
				System.out.println("invalid patient id");
				options();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
