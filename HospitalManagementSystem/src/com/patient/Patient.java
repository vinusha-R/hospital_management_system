package com.patient;

public class Patient {
	int id, docid;
	String name, dept, status, pwd, date;

	public Patient(int id, String name, String dept, String status, int docid, String pwd, String date) {
		this.id = id;
		this.name = name;
		this.dept = dept;
		this.status = status;
		this.docid = docid;
		this.pwd = pwd;
		this.date = date;
	}

}
