package com.admin;

import java.sql.*;
import java.sql.Date;
import java.util.*;

import com.database.Database;
import com.doctor.DoctorOptions;
import com.hms.HospitalSvims;
import com.hms.IdGenerator;

public class AdminOptions extends IdGenerator {
	Scanner scanner = new Scanner(System.in);
	Connection con = Database.connectDb();
//	public static int newId;
//	
//	public int idGeneration() {
//		try {
//			//Statement 
//		}catch(Exception e) {
//			
//		}
//		
//	}

	public void display() {
		System.out.println("choose your option");
		System.out.println("1:Login");
		System.out.println("2:Register");
		int option = scanner.nextInt();
		switch (option) {
		case 1:
			login();
			break;
		case 2:
			register();
			break;
		}
	}

	public void login() {
		String pwd = null;
		String password = null;
		System.out.println("enter your id");
		String myId = scanner.next();
		ArrayList<String> al = new ArrayList();
		Connection con = Database.connectDb();
		try {
			Statement statement = con.createStatement();
			ResultSet adminResultList = statement.executeQuery("select * from admin");
			while (adminResultList.next()) {
				al.add(adminResultList.getString(1));
			}
			if (al.contains(myId)) {
				ResultSet adminResult = statement.executeQuery("select * from admin where aid='" + myId + "'");
				adminResult.next();
				pwd = adminResult.getString(3);
				System.out.println("enter your password");
				password = scanner.next();

			} else {
				System.out.println("invalid id");
				System.out.println("enter valid id or if new user register");
				display();
			}
			if (password.equals(pwd)) {
				System.out.println("welcome");
				while (true) {
					options();
				}
			} else {
				System.out.println("invalid password");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void register() {
		System.out.println("enter name");
		String name = scanner.next();
		IdGenerator adminId = new AdminOptions();
		int id = adminId.generateId("admin");
		String newId = "a" + id;
//		System.out.println("enter id");
//		int id = sc.nextInt();
		System.out.println("enter password");
		String pwd = scanner.next();
		// Admin admin = new Admin(id, pwd, name);
		try {
			Statement statement = con.createStatement();
			int adminDetails = statement.executeUpdate(
					"insert into admin(aid,aname,apwd)values('" + newId + "','" + name + "','" + pwd + "')");
			if (adminDetails != 0) {
				System.out.println("Your registration has been successful");
				System.out.println("your id is :" + newId);
				System.out.println("login here");
				login();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void options() {
		System.out.println("choose your option");
		System.out.println("1:Schedule Patient");
		System.out.println("2:Exit");
		// System.out.println("2:Generate Patient report");
		int option = scanner.nextInt();
		switch (option) {
		case 1:
			schedulePatient();
			break;
		case 2:
			HospitalSvims.svimsDisplay();
//		case 2:
//			generateReport();
//			break;
		}
	}

	public void schedulePatient() {
		Statement statement;
		try {
			statement = con.createStatement();
			ResultSet pateintDoctorList = statement.executeQuery(
					"select pid,status,pdate,doctor.docid,ddate from doctor inner join patient where (doctor.docid = patient.docid and patient.status =false)");
			if (pateintDoctorList.next()) {
				pateintDoctorList.beforeFirst();
				System.out.println("patients to be scheduled are");
				// System.out.println("pid" + " || " + "status" + " || " + "date");
				System.out.format("%10s%10s%10s\n", "pid", "status", "date");
				System.out.println("-------------------------------");
				while (pateintDoctorList.next()) {
					// System.out.println(rs.getString(1) + " || " + rs.getBoolean(2) + " || " +
					// rs.getString(3));
					System.out.format("%10s%10s%15s\n", pateintDoctorList.getString(1), pateintDoctorList.getBoolean(2), pateintDoctorList.getString(3));
				}
				pateintDoctorList.beforeFirst();
				while (pateintDoctorList.next()) {
					System.out.println("enter the above pid to schedule the patient");
					String patientId = scanner.next();
					Statement statementNew = con.createStatement();
					int patientUpdateResult = statementNew.executeUpdate("update patient set status= true where pid='" + patientId + "'");
					if (patientUpdateResult != 0) {
						System.out.println("patient has been scheduled and patient id is:" + patientId);
					}
					System.out.println("Yout want to stop scheduling patient..type Yes or No");
					String keyPressed = scanner.next();
					if (keyPressed.equals("Yes")) {
						options();
					} else {
						continue;
					}
				}
				System.out.println("all patients have been scheduled");
				options();

			} else {
				System.out.println("no patients to schedule today");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	public void schedulePatient() {
//
//		Statement st;
//		try {
//			st = con.createStatement();
//			// String s = "No";
//			ResultSet rs = st.executeQuery(
//					"select pid,status,pdate,doctor.docid,ddate from doctor inner join patient where (doctor.docid = patient.docid and patient.status =false)");
//			if (rs.next()) {
//				rs.beforeFirst();
//				System.out.println("patients to be scheduled are");
////				System.out.println("pid" + " || " + "status" + " || " + "date");
//				System.out.format("%10s%10s%10s\n", "pid","status","date");
//				System.out.println("-------------------------------");
//				while (rs.next()) {
////					System.out.println(rs.getInt(1) + " || " + rs.getString(2) + "     || " + rs.getString(3));
//					System.out.format("%10s%10s%15s\n",rs.getString(1),rs.getBoolean(2),rs.getString(3));
//					Statement st1 = con.createStatement();
//					int a = st1.executeUpdate("insert into patientSchedule(pid,status,pdate,docid,ddate)values('"
//							+ rs.getString(1) + "'," + rs.getBoolean(2) + ",'" + rs.getString(3) + "','" + rs.getString(4)
//							+ "','" + rs.getString(5) + "')");
//					if(a!=0) {
//						System.out.println("patient has been scheduled and patient id is:"+rs.getString(1));
//					}
//				}
//				ResultSet rs1 = st.executeQuery("select * from patientSchedule");
//				while (rs1.next()) {
//					String date1 = rs1.getString(3);
//					Date dateNew1 = Date.valueOf(date1);
//					String date2 = rs1.getString(5);
//					Date dateNew2 = Date.valueOf(date2);
//					if ((dateNew1.compareTo(dateNew2)) == 0) {
//						// if (rs1.getString(3).equals(rs1.getString(4))) {
//						Statement st1 = con.createStatement();
//						int a1 = st1.executeUpdate("update patient set status= true where pid='" + rs1.getString(1)+"'");
//						// int a2 = st.executeUpdate("delete from patientSchedule where
//						// pid="+rs.getInt(i++));
//					}
//				}
//
//			} else {
//				System.out.println("No patients to schedule");
//			}
//
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

//	public void generateReport() {
//		DoctorOptions d = new DoctorOptions();
//		ArrayList al = d.returnReport();
//		if (al == null) {
//			System.out.println("Report has not been sent by doctor");
//		} else {
//			int id = (int) al.get(0);
//			String status = (String) al.get(1);
//			String pbs = (String) al.get(2);
//			String cbcs = (String) al.get(3);
//			String antigen = (String) al.get(4);
//			System.out.println("enter report id");
//			int reportId = sc.nextInt();
//			try {
//				Statement st = con.createStatement();
//				int a = st.executeUpdate("insert into patientReport(reportId,diagnosed,pbs,cbcs,antigen,pid)values("
//						+ reportId + "," + status + "," + pbs + "," + cbcs + "," + antigen + "," + id + ")");
//				int a1 = st.executeUpdate(
//						"insert into patientList(pid,pname,department,status,docid,ppwd,pdate)select * from patient where pid="
//								+ id);
//				// int a2 = st.executeUpdate("delete from patient where pid="+id);
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//		}
//	}
}
