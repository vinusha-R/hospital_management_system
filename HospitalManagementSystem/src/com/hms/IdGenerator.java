package com.hms;

import java.sql.*;

import com.database.Database;

public abstract class IdGenerator implements Account {
	Connection con = Database.connectDb();
	public int newId;

	public abstract void login();

	public abstract void register();

	public int generateId(String tableName) {
		try {
			Statement statement = con.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from " + tableName);
			resultSet.last();
			// System.out.println(rs.last());
			String lastId = resultSet.getString(1);
			// System.out.println(lastId);
			int id = Integer.parseInt(lastId.substring(1));
			return ++id;

		} catch (Exception e) {
			return 1;
		}

	}

}
