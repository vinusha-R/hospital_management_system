package com.hms;

import java.util.Scanner;

import com.admin.AdminOptions;
import com.doctor.DoctorOptions;
import com.patient.PatientOptions;

public class HospitalSvims {
	public static void main(String[] args) {
		svimsDisplay();
	}

	public static void svimsDisplay() {
		while (true) {
			System.out.println("Welcome to SVIMS");
			System.out.println("choose your option");
			System.out.println("1:Admin");
			System.out.println("2:Doctor");
			System.out.println("3:Patient");
			System.out.println("4:Exit");
			Scanner scanner = new Scanner(System.in);
			int option = scanner.nextInt();
			switch (option) {
			case 1:
				AdminOptions adminOptions = new AdminOptions();
				adminOptions.display();
				break;
			case 2:
				DoctorOptions doctorOptions = new DoctorOptions();
				doctorOptions.display();
				break;
			case 3:
				PatientOptions patientOptions = new PatientOptions();
				patientOptions.display();
				break;
			case 4:
				System.exit(0);
			}
		}
	}

}
