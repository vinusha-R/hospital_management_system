package com.doctor;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

import com.admin.AdminOptions;
import com.database.Database;
import com.hms.Account;
import com.hms.HospitalSvims;
import com.hms.IdGenerator;
import com.patient.PatientOptions;

public class DoctorOptions extends IdGenerator {
	Scanner scanner = new Scanner(System.in);
	Connection con = Database.connectDb();
	int id = 0;

	public void display() {
		System.out.println("choose your option");
		System.out.println("1:Login");
		System.out.println("2:Register");
		int option = scanner.nextInt();
		switch (option) {
		case 1:
			login();
			break;
		case 2:
			register();
			break;
		}
	}

	public void login() {
		String pwd = null;
		String password = null;
		System.out.println("enter your id");
		String myId = scanner.next();
		ArrayList<String> arrayList = new ArrayList();
		Connection con = Database.connectDb();
		try {
			Statement statement = con.createStatement();
			ResultSet doctorList = statement.executeQuery("select * from doctor");
			while (doctorList.next()) {
				arrayList.add(doctorList.getString(1));
			}
			if (arrayList.contains(myId)) {
				ResultSet doctorResultSet = statement.executeQuery("select * from doctor where docid='" + myId + "'");
				// System.out.println(rs1.next());
				doctorResultSet.next();
				pwd = doctorResultSet.getString(5);
				System.out.println("enter your password");
				password = scanner.next();
				// System.out.println(pwd);
			} else {
				System.out.println("invalid id");
				System.out.println("enter valid id or if new user register");
				display();
			}
			if (password.equals(pwd)) {
				System.out.println("welcome");
				while (true) {
					options();
				}
			} else {
				System.out.println("invalid password");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void register() {
//		System.out.println("enter your id");
//		id = sc.nextInt();
		IdGenerator adminId = new AdminOptions();
		int id = adminId.generateId("doctor");
		String newId = "d" + id;
		System.out.println("enter your name");
		String name = scanner.next();
		System.out.println("enter your specialization");
		String specialization = scanner.next();
		System.out.println("enter password");
		String pwd = scanner.next();
		System.out.println("enter available date");
		String docDate = scanner.next();
		// Date dDateNew = Date.valueOf(docDate);
		try {
//			Statement st = con.createStatement();
//			int a = st.executeUpdate("insert into doctor(docid,docName,specialization,ddate,docpwd)values(" + id + ",'"
//					+ name + "','" + specialization + "','null'" +"'"+ pwd +"'"+ ")");
			String sql = "insert into doctor(docid,docName,specialization,ddate,docpwd,patientChecked)values(?,?,?,?,?,?)";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, newId);
			ps.setString(2, name);
			ps.setString(3, specialization);
			ps.setString(4, docDate);
			ps.setString(5, pwd);
			ps.setBoolean(6, false);
			int status = ps.executeUpdate();
			if (status != 0) {
				System.out.println("Your registration has been done");
				System.out.println("your id is :" + newId);
				System.out.println("login here");
				login();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void options() {
		System.out.println("choose your option");
		System.out.println("1:View patient list");
		System.out.println("2:Update available dates");
		System.out.println("3:Generate patient report");
		System.out.println("4:View patient report");
		System.out.println("5:Exit");
		int option = scanner.nextInt();
		switch (option) {
		case 1:
			viewPatient();
			break;
		case 2:
			updateAvailableDate();
			break;
		case 3:
			System.out.println("enter patient id");
			String pid = scanner.next();
			generateReport(pid);
			break;
		case 4:
			PatientOptions patientOptions = new PatientOptions();
			System.out.println("enter patient id");
			String patientId = scanner.next();
			patientOptions.viewReport(patientId);
			break;
		case 5:
			HospitalSvims.svimsDisplay();
		}
	}

	public void viewPatient() {
		try {
//			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
//			Date date = new Date(0);
			// System.out.println(date);
//			System.out.println(java.time.LocalDate.now());  
			LocalDate todayDate = java.time.LocalDate.now();
			Statement statement = con.createStatement();
			ResultSet resultSet = statement.executeQuery(
					"select * from patient where status=true and pdate='" + todayDate + "' and checkupDone = false");
//			rs.next();
//			System.out.println(rs.getInt(1));
			if (resultSet.next()) {
//				System.out.println("pid" + "   " + "pname");
//				System.out.println(rs.getInt(1) + "   " + rs.getString(2));
				System.out.format("%10s%10s\n", "pid", "pname");
				System.out.println("-------------------------");
				resultSet.beforeFirst();
				while (resultSet.next()) {
//					System.out.println(rs.getInt(1) + "   " + rs.getString(2));
					System.out.format("%10s%10s\n", resultSet.getString(1), resultSet.getString(2));
				}
				resultSet.beforeFirst();
				while (resultSet.next()) {
					System.out.println("select the above listed patient id for check up");
					String patient_id = scanner.next();
					generateReport(patient_id);
					System.out.println("Yout want to stop checking up the patient..type Yes or No");
					String keyPressed = scanner.next();
					if (keyPressed.equals("Yes")) {
						options();
					} else {
						continue;
					}
				}
				System.out.println("all patients have been checked for today");

			} else {
				System.out.println("No patients for today");
			}
//			rs.beforeFirst();
//			while(rs.next()) {
//				int a = st.executeUpdate("insert into patientlist(pid,pname,department,status,docid,ppwd,pdate)values
//			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void updateAvailableDate() {
		try {
			Statement statement = con.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from doctor where docid=" + id);
			resultSet.next();
//			System.out.println(rs.getString(4));
//			System.out.println("hello");
//			if((rs.getString(4))==null) {
//				System.out.println("hi");
//			}
//			if((rs.getString(4)).equals(null)) {
			if ((resultSet.getString(4)) == null) {
				System.out.println("enter the available date");
				String date = scanner.next();
				// Statement st = con.createStatement();
				// int a = st.executeUpdate("update doctor set ddate="+date+"where docid="+id);
				String updateDate = "update doctor set ddate=? where docid=?";
				PreparedStatement preparedStatement = con.prepareStatement(updateDate);
				preparedStatement.setString(1, date);
				preparedStatement.setInt(2, id);
				int status = preparedStatement.executeUpdate();
				if (status != 0) {
					System.out.println("Available date has been updated");
				} else {
					System.out.println("failed to update try again");
					updateAvailableDate();
				}
			} else if ((resultSet.getString(6)).equals("Yes")) {
				System.out.println("enter the available date to update with");
				String date1 = scanner.next();
				// int a = st.executeUpdate("update doctor set ddate="+date1+"where docid="+id);
				String updateDate = "update doctor set ddate=? where docid=?";
				PreparedStatement preparedStatement = con.prepareStatement(updateDate);
				preparedStatement.setString(1, date1);
				preparedStatement.setInt(2, id);
				int status = preparedStatement.executeUpdate();
				if (status != 0) {
					System.out.println("Available date has been updated");
				} else {
					System.out.println("failed to update try again");
					updateAvailableDate();
				}
			} else {
				resultSet.beforeFirst();
				resultSet.next();
				System.out.println("The present available date is: " + resultSet.getString(4));
				System.out.println("As patients are scheduled on this day...You are not supposed to change this date");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void generateReport(String patient_id) {
//		System.out.println("patient id");
//		int id = sc.nextInt();
		try {
			Statement statement = con.createStatement();
			ResultSet patientReportResult = statement.executeQuery("select * from patientReport");
			Statement statementNew = con.createStatement();
			ResultSet patientResult = statementNew.executeQuery("select * from patient");
			ArrayList<String> patientIdList = new ArrayList<>();
			ArrayList<String> patientReportIdList = new ArrayList<>();
			if(patientReportResult.next() == true) {
			while (patientReportResult.next()) {
				patientReportIdList.add(patientReportResult.getString(6));
			}
			try {
				while (patientResult.next()) {
					patientIdList.add(patientReportResult.getString(1));
				}
			} catch (SQLException e) {
				// to solve illegal operation on empty result set when patientreport is empty
				setReportValues(patient_id);
			}
			if (patientIdList.contains(patient_id)) {
				if (patientReportIdList.contains(patient_id)) {
					System.out.println("report is already generated view the report");
					PatientOptions patientOptions = new PatientOptions();
					patientOptions.viewReport(patient_id);
				} else {
					setReportValues(patient_id);
				}
			} else {
				System.out.println("invalid id");

			}
			}else {
				setReportValues(patient_id);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void setReportValues(String patient_id) {
		IdGenerator reportId = new DoctorOptions();
		int id = reportId.generateId("patientReport");
		String newReportId = "R" + id;
		System.out.println("Is diagnosed");
		String diagnosed = scanner.next();
		System.out.println("enter PBS");
		String pbs = scanner.next();
		System.out.println("enter CBCS");
		String cbcs = scanner.next();
		System.out.println("enter Antigen");
		String antigen = scanner.next();
		try {
			String sql = "insert into patientReport(reportId,diagnosed,pbs,cbcs,antigen,pid)values(?,?,?,?,?,?)";
			PreparedStatement updatePatientReport = con.prepareStatement(sql);
			updatePatientReport.setString(1, newReportId);
			updatePatientReport.setString(2, diagnosed);
			updatePatientReport.setString(3, pbs);
			updatePatientReport.setString(4, cbcs);
			updatePatientReport.setString(5, antigen);
			updatePatientReport.setString(6, patient_id);
			int reportStatus = updatePatientReport.executeUpdate();
			String checkupDone = "update patient set checkupDone = ? where pid = ?";
			PreparedStatement patientUpdate = con.prepareStatement(checkupDone);
			patientUpdate.setBoolean(1, true);
			patientUpdate.setString(2, patient_id);
			int patientCheckedStatus = patientUpdate.executeUpdate();
			if ((reportStatus != 0) && (patientCheckedStatus != 0)) {
				System.out.println(patient_id + " :patient is checked and report has been generated");
			} else {
				System.out.println("some error in generating report..generate the report again");
				generateReport(patient_id);
			}

//			int a = st1.executeUpdate("insert into patientReport(reportId,diagnosed,pbs,cbcs,antigen,pid)values("
//					+ reportId + "," + d + "," + pbs + "," + cbcs + "," + antigen + "," + patient_id + ")");
//			Statement st1 = con.createStatement();
//			int a1 = st1.executeUpdate(
//					"insert into patientList(pid,pname,department,status,docid,ppwd,pdate)select * from patient where pid="
//							+ patient_id);
//		System.out.println(a1);
//			Statement st2 = con.createStatement();
//			int a2 = st2.executeUpdate("delete from patient where pid=" + patient_id);
//			System.out.println(a2);
//			String updatePatientChecked = "update doctor set patientChecked = ? where docid = ?";
//			PreparedStatement preparedStatement = con.prepareStatement(updatePatientChecked);
//			preparedStatement.setBoolean(1, true);
//			preparedStatement.setInt(2, id);
//			preparedStatement.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}

//	public void patientReport() {
//		ArrayList al = new ArrayList();
//		System.out.println("patient id");
//		int id = sc.nextInt();
//		System.out.println("Is diagnosed");
//		String d = sc.next();
//		System.out.println("enter PBS");
//		String pbs = sc.next();
//		System.out.println("enter CBCS");
//		String cbcs = sc.next();
//		System.out.println("enter Antigen");
//		String antigen = sc.next();
//	}
//	public ArrayList returnReport() {
//		return al;
//	}
//
//	public void viewPatientReport() {
//		System.out.println("enter patient id");
//		int pid = sc.nextInt();
//		try {
//			Statement st = con.createStatement();
//			ResultSet rs = st.executeQuery("select * from patientReport where pid=" + pid);
//			if (rs.next()) {
//				System.out.println(
//						"ReportId" + " " + "Diagnosis" + " " + "PBS" + " " + "CBCS" + " " + "Antigen" + " " + "pid");
//				System.out.println(rs.getInt(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4)
//						+ " " + rs.getString(5) + " " + rs.getInt(6));
//				while (rs.next()) {
//					System.out.println(rs.getInt(1) + " " + rs.getString(2) + " " + rs.getString(3) + " "
//							+ rs.getString(4) + " " + rs.getString(5) + " " + rs.getInt(6));
//				}
//			}else {
//				System.out.println("Patient report has not been generated");
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//}
